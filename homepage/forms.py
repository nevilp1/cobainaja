from . import models
from django import forms
import datetime

class StatusForm(forms.ModelForm):
    isi = forms.CharField(widget=forms.TextInput(attrs={
                "required" : True,
                "placeholder":"Nama Status",
                }))
    tanggal = forms.DateTimeField(widget=forms.DateTimeInput(attrs={
    	"required" : True,
    	"placeholder" : datetime.datetime.now()
    	}))
   
    class Meta:
        model = models.Status
        fields = ["isi", "tanggal"]