from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from django.utils import timezone
from . import models
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

class UnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_using_to_do_list_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'home.html')

	def test_using_index(self):
		found = resolve('/')
		self.assertEqual(found.func, views.landing)

	def test_model_create(self):
		new_status = Status.objects.create(isi = 'karin patricia', tanggal=timezone.now())
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)

	def test_form_is_post(self):
		response = Client().post('/', data={
			'isi' : 'Hello!',
			'tanggal' : '2019-10-30 18:00'
			})
		self.assertEqual(response.status_code, 302)

	def test_is_valid(self):
		response = StatusForm(data={
			'isi' : 'Hello!',
			'tanggal' : '2019-10-30 18:00'
			})
		self.assertTrue(response.is_valid())







# Create your tests here.
